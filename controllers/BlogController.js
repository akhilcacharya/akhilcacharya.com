module.exports = function(config, mongoose) {
    var moment = require('moment');
    var util = require('util');
    var md = require('node-markdown').Markdown;
    var Blogpost = mongoose.model('Post');
    var slugger = require('slugger');

    var routes = {
        home: function(req, res) {
            //Get all posts
            Blogpost.find({}, function(err, posts) {
                if (posts.length != 0) {
                    res.render('bloghome', {
                        posts: posts.reverse(),
                        md: md,
                        moment: moment
                    });
                }
            });
        },
        login: function(req, res){
            //Render Login Page
            res.render('login');
        },

        authenticate: function(req, res){
            //Accept HTTP POST data and authenticate
            var username = req.body.username;
            var password = req.body.password;
            if (username == config.user_id && password == config.password) {
                req.session.loggedin = true;
                res.redirect("/admin");
            } else {
                res.redirect("/login");
            }
        },

        logout: function(req, res) {
            //Delete the session by setting it to undefined
            delete req.session.loggedin;
            res.redirect('/');
        },

        post: function(req, res){
            //Get and render individual post
            var queryid = req.params.id;
            Blogpost.find({
                guid: queryid
            }, function(err, post) {
                if (err) {
                    throw err;
                    res.render('404');
                } else if (post.length == 0) {
                    res.render('404');
                } else if (post != undefined) {
                    res.render('blogpost', {
                        post: post[0],
                        md: md,
                        moment: moment
                    });
                }
            });
        },

        deletepost: function(req, res){
            //Delete the post by accepting the GET request. Redirect to the admin page. 
            var queryid = req.params.id;
            Blogpost.find({
                guid: queryid
            }).remove();
            console.log('Post ' + queryid + ' deleted');
            res.redirect('/admin');
        },

        updatepost: function(req, res){
            //Accept POST and save the post. 
            var queryid = req.body.id;
            Blogpost.findOne({
                guid: queryid
            }, function(err, post) {
                if (post) {
                    post.content = req.body.content;
                    post.save(function(err) {
                        if (!err) {
                            res.send({
                                success: true,
                                statuscode: 200
                            });
                        } else {
                            console.log(err);
                            res.send({
                                success: false,
                                statuscode: 400
                            });
                        }
                    });
                }
            });

        },

        editpost: function(req, res) {
            //Render the editpost page. 
            var queryid = req.params.id;
            Blogpost.find({
                guid: queryid
            }, function(err, post) {
                res.render('editpost', {
                    post: post[0]
                });
            });
        },

        admin: function(req, res){
            //Render the admin page. 
            Blogpost.find({}, function(err, posts) {
                if (posts) {
                    res.render('admin', {
                        posts: posts.reverse()
                    });
                } else {
                    console.log("error");
                    res.render('404');
                }
            });
        },

        createpost: function(req, res) {
            //Render new post 
            res.render('newpost');
        },

        newpost: function(req, res){
            //Save the POSTed data to post. 
            var post = req.body;
            var newPost = Blogpost({
                title: post.title,
                content: post.content,
                guid: routes.getId(),
                slug: slugger(post.title),
            });

            newPost.save(function(err, post) {
                if (err) {
                    res.send({success: false});
                    console.log(util.inspect(err));
                } else {
                    post.saveCallback();
                    res.send({success: true});
                }
            });
        },
        getId: function(req, res) {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            return text;
        }
    };

    return routes;
}
