var mongoose = require('mongoose');

var postSchema = mongoose.Schema({
    title: String,
    slug: String,
    guid: String,
    url: String,
    date: {
        type: Date,
        default: Date.now,
    },
    content: String,
});

postSchema.methods.saveCallback = function() {
    console.log("Posted saved!");
}

mongoose.model("Post", postSchema);
