#AkhilCAcharya.com

When I was learning Node, Express, and Mongo I decided to make a personal site. This is the result. 

I operated the site for a few months on Elastic Beanstalk until I realized that AWS for Node hosting is far more expensive than it was worth. So now it's here  ¯\\_(ツ)_/¯.