$(function(){		
	var post_create_url = "/admin/newpost"; 
	$("#post_submit").click(function(){
		var title_ = $("#title").val(); 
		var content_ = $("#post_content").val(); 
		$.ajax({
			url: post_create_url, 
			type: "post", 
			data: {title: title_, content: content_}, 
			dataType: "json", 
			success: function(data){
				if(data.success){
					window.location = "/admin"; 
				}else{
					window.location.reload(); 
				}
			}
		}); 
	}); 
}); 
