$(function(){		
	var post_create_url = "/admin/updatepost"; 
	var post_guid = $(location).attr('href').split('/')[5]; 
	$("#post_submit").click(function(){
		var content_ =$("#post_content").val(); 
		$.ajax({
			url: post_create_url, 
			type: "post", 
			data: {content: content_, id: post_guid}, 
			dataType: "json", 
			success: function(data){
				if(data.success){
					window.location = "/admin"; 
				}else{
					window.location.reload(); 
				}
			}, 
			error: function(jqXHR, status, error){
				console.log(error); 
			}, 
		}); 
	}); 
}); 