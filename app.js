/*****************************
 * Author: Akhil Acharya
 * Project: akhilcacharya.tk
 ******************************/

var express = require('express');

//Configuration
var config = require(__dirname + '/config.json');

var app = module.exports = express.createServer();

//Database
var mongoose = require('mongoose');
var mongo_url = config.db.base_url + "" + config.db.username + ':' + config.db.password + config.db.path;
mongoose.connect(mongo_url);

var db = mongoose.connection;
db.on('error', console.error.bind(console, "Connection error"));
db.once('open', function callback() {
    console.log("Connected to server");
});

//Models
//Instantiate model
require('./models/blogpost.js')


//Controllers
var indexController = require('./controllers/IndexController.js');
var blogController = require("./controllers/BlogController.js")(config, mongoose);

// Configuration
app.configure(function() {
    app.use(express.bodyParser());
    app.use(express.cookieParser());
    app.use(express.session({
        secret: "I'm actually Sri Lankan"
    }));
    app.set('views', __dirname + '/views');
    app.set('view engine', 'jade');
    app.use(app.router);
    app.use(express.static(__dirname + '/public'));
});

app.configure('development', function() {
    app.use(express.errorHandler({
        dumpExceptions: true,
        showStack: true
    }));
});

app.configure('production', function() {
    app.use(express.errorHandler());
});

var isAuthenticated = function(req, res, next) {
    if(req.session.loggedin == undefined){
          res.redirect('/'); 
    }else{
        next(); 
    }
}

//Public GET
app.get('/', indexController.index);
app.get('/login', blogController.login);
app.get('/logout', blogController.logout);
app.get("/blog", blogController.home);
app.get('/blog/:id', blogController.post);

//Authenticated GET
app.get('/admin', isAuthenticated, blogController.admin);
app.get("/admin/createpost", isAuthenticated, blogController.createpost);
app.get('/admin/delete/:id', isAuthenticated, blogController.deletepost);
app.get('/admin/edit/:id', isAuthenticated, blogController.editpost);


//Authenticated POST -- todo, actual authentication
app.post("/admin/newpost", blogController.newpost);
app.post('/login', blogController.authenticate);
app.post("/admin/updatepost", blogController.updatepost)


app.listen(8081, function() {
    console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});


